/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//(function ( $, Drupal, window, document, undefined ) {
//
//  Drupal.behaviors.passIdstoExportForm = {
//    attach: function (context, settings) {
//       //alert('entered');
//       
//        console.log(ids)
//    }
//  };
//})(jQuery, Drupal, this, this.document);


(function ($, Drupal) {
    $('input[name="redirect_url"]').val(window.location.href);
    $('td.views-field-commerce-product-bulk-form .form-checkbox').change(function() {
           var ids = '';
           $('td.views-field-commerce-product-bulk-form .form-checkbox').each(function () {
            var product_id = ($(this).closest('td').siblings('td.views-field-product-id').html());
            if (this.checked && product_id != '') {
              if (ids == '') {
                ids += product_id;
              }
              else {
                ids += ','+product_id;
              }
            }
           });
           $('input[name="ids_seperated_by_comma"]').val(ids);
        });
    $('td.views-field-node-bulk-form .form-checkbox').change(function() {
           var ids = '';
           $('td.views-field-node-bulk-form .form-checkbox').each(function () {
            var product_id = ($(this).closest('td').siblings('td.views-field-nid').html());
            if (this.checked && product_id != '') {
              if (ids == '') {
                ids += product_id;
              }
              else {
                ids += ','+product_id;
              }
            }
           });
           $('input[name="ids_seperated_by_comma"]').val(ids);
        });    
})(jQuery, Drupal);

