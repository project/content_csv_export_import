CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module is an extension of commerce_smart_importer module which helps in importing both nodes and products.The images and excel files for each bundle are zipped and downloaded during export.

REQUIREMENTS
------------

Drupal commerce module

INSTALLATION
------------

Install the module from "admin/extend" or using drush.

CONFIGURATION
------------

This can be imported in other environment to create or update the Nodes/Products.
By default, the export can be done using IDs (seperated by comma) at "/admin/export-content-by-ids" and the data can be imported at "/admin/import-content-by-csv".

The form can also be configured to listing pages as below:
1. Add Node Id or Product Id in the listing view.
2. Implement below hooks in your theme to prepare form variable.
/**
 * Implements hook_preprocess_HOOK() for page.
 */
function <theme>_preprocess_page(&$variables) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  if ($route_name == 'entity.commerce_product.collection') {
   $variables['export_csv_form'] = \Drupal::formBuilder()->getForm('\Drupal\content_csv_export_import\Form\ExportContentByIdsForm');
  }
  if ($route_name == 'system.admin_content') {
   $variables['export_csv_form'] = \Drupal::formBuilder()->getForm('\Drupal\content_csv_export_import\Form\ExportContentByIdsForm');
  }
}
3. Now render this form in your views template.
4. Once changed clear the cache and you will see a button on the view pages.

Export:
1.	Go to “/admin/content” or “/admin/commerce/products” and select the entities you want to export.
2.	Click on “Export the selected to CSV” button.
3.	The selected entities would be downloaded as zip.
Import:
1.	Go to “/admin/import-content-by-csv” and upload the zip
2.	Select the appropriate entity type and Operation mode.
3.	Once submitted, the entities will be imported.

MAINTAINERS
-----------
* Challa Sravya (sravya_12) - https://www.drupal.org/u/sravya_12
