<?php

namespace Drupal\content_csv_export_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\content_csv_export_import\ContentExportImportCoreService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
use Drupal\content_csv_export_import\ImportingParameters;

/**
 * Class ContentImportForm.
 */
class ContentImportForm extends FormBase {

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Smart importer service.
   *
   * @var \Drupal\content_csv_export_import\Plugin\ContentExportImportCoreService
   */
  protected $contentDataExportService;
  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * CSVExportForm constructor.
   */
  public function __construct(Connection $connection,
                              ContentExportImportCoreService $contentDataExportService,
                              ConfigFactory $configFactory,
                              EntityTypeManagerInterface $entityTypeManager
                              ) {
    $this->database = $connection;
    $this->contentExportImportService = $contentDataExportService;
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;

  }

  /**
   * Create.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('content_csv_export_import.core'),
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_csv_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $default_import_folder = 'public://import-entities';
    if (!is_dir($default_import_folder)) {
      \Drupal::service('file_system')->mkdir($default_export_folder, 0755);
    }
    $form['upload_entities_zip_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload zip file'),
      '#required' => TRUE,
      '#description' => $this->t('Upload content zipped csv files'),
      '#weight' => '0',
      '#upload_validators' => [
        'file_validate_extensions' => ['zip'],
      ],
      '#upload_location' => $default_import_folder,
    ];
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose entity type'),
      // '#default_value' => 'node',
      '#options' => [
        'node' => $this->t('Node'),
        'commerce_product' => $this->t('Product'),
      ],
      '#required' => TRUE,
    ];
    $form['operation_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose operation mode'),
      // '#default_value' => 'create',
      '#options' => [
        'create' => 'Create',
        'update' => 'Update',
      ],
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @todo Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $content_zip_file = $form_state->getValue('upload_entities_zip_file', 0);
    $entity_type = $form_state->getValue('entity_type');
    $operation_mode = $form_state->getValue('operation_mode');
    if (isset($content_zip_file[0]) && !empty($content_zip_file[0])) {
      $content_file = File::load($content_zip_file[0]);
      $content_file->setPermanent();
      $content_file->save();
      $import_dir = \Drupal::service('file_system')->realpath('public://import-entities/');
      $abs_path_file = $import_dir . '/' . $content_file->getfileName();
      // Extract the zip file to a folder.
      $zip = new \ZipArchive();
      $res = $zip->open($abs_path_file);
      $zip_folder_pathinfo = pathinfo($content_file->getfileName());
      $extract_folder_path = $import_dir . '/' . $zip_folder_pathinfo['filename'];
      $uri_folder_name = $save = "public://import-entities/" . $zip_folder_pathinfo['filename'];
      $new_folder = mkdir($extract_folder_path, 0755, TRUE);
      chmod($extract_folder_path, 0755);
      if ($res === TRUE) {
        $zip->extractTo($extract_folder_path);
        $zip->close();
        // Setting the import parameters.
        $parameters = $importParameters = new ImportingParameters();
        $importParameters->disableAll();
        $importParameters->createNode = TRUE;
        $importParameters->createProduct = TRUE;
        $importParameters->defaultValues = TRUE;
        $importParameters->duplicateValues = TRUE;
        $parameters = $importParameters;

        $csv_file_paths = [];
        $import_arguments = [];
        $batch = [
          'title' => $this->t('Importing selected entities'),
          'init_message' => $this->t('Beginning...'),
          'progress_message' => $this->t('Imported @current out of @total entities'),
          'error_message' => $this->t('Something went wrong'),
          'progressive' => FALSE,
          'operations' => [],
          'finished' => [$this, 'importFinishedCallback'],
        ];

        $all_entities = [];
        // Copy inline images to default inline images path.
        $extract_full_folders = scandir($extract_folder_path);
        $extract_files = array_diff($extract_full_folders, ['.', '..']);

        $inline_files_source_path = $extract_folder_path . '/inline-images';

        $inline_image_folders = scandir($extract_folder_path . '/inline-images');
        $inline_image_files = array_diff($inline_image_folders, ['.', '..']);
        $csv_files = [];
        $uri_folder_name = $save = "public://import-entities/" . $zip_folder_pathinfo['filename'];

        $external_folders = array_merge([$save], []);

        $inline_image_path = \Drupal::service('file_system')->realpath('public://inline-images/');

        foreach ($inline_image_files as $inline_image_file) {
          copy($inline_files_source_path . '/' . $inline_image_file,
            $inline_image_path . '/' . $inline_image_file);
        }
        // Get all CSV files.
        foreach ($extract_files as $extract_file) {
          $extract_abs_path = \Drupal::service('file_system')->realpath($uri_folder_name . '/' . $extract_file);
          $extract_file_ext = pathinfo($extract_abs_path, PATHINFO_EXTENSION);
          if ($extract_file_ext == 'csv') {
            $csv_files[] = $extract_file;
          }
        }
        $import_results_log = [];
        if ($entity_type == 'node') {
          $import_type = 'node';
          $sub_type = 'paragraph';
        }
        else {
          $import_type = 'product';
          $sub_type = 'variation';
        }
        $subtype_plural = $sub_type . 's';
        // Prepare the batch process with contents from each CSV file.
        foreach ($csv_files as $csv_file) {
          $split_filename = explode('-', $csv_file);
          $uri = $extract_folder_path . '/' . $csv_file;
          // kint($uri);exit;
          $count = $this->contentExportImportService->countEntitiesAndSubEntities($uri);
          // Indexing labels.
          $csvData = fopen($uri, 'r');
          $headers = fgetcsv($csvData);
          fclose($csvData);
          $bundle = $split_filename[count($split_filename) - 2];
          $entity_type = $form_state->getValue('entity_type');
          $fields = $this->contentExportImportService->getFieldDefinition(FALSE, $bundle, $entity_type, $operation_mode);
          // kint($fields);exit;
          // Set indexes for each field.
          foreach ($headers as $index => $header) {
            foreach ($fields[$import_type] as $key => $field) {
              if ($field['label'] == $header) {
                $fields[$import_type][$key]['index'] = $index;
              }
            }
            if ($import_type == 'node' && isset($fields[$sub_type])) {
              foreach ($fields[$sub_type] as $sub_type_bundle => $bundleFieldData) {
                foreach ($bundleFieldData as $key => $field) {
                  if ($field['label'] == $header) {
                    $fields[$sub_type][$sub_type_bundle][$key]['index'] = $index;
                  }
                }
              }
            }
            else {
              foreach ($fields[$sub_type] as $key => $field) {
                if ($field['label'] == $header) {
                  $fields[$sub_type][$key]['index'] = $index;
                }
              }
            }
          }

          if ($import_type == 'node') {
            $first_subtype_index = $this->getFirstSubTypeIndex($fields, $sub_type, $import_type);
          }
          else {
            $first_subtype_index = $this->getFirstSubTypeIndex($fields, $sub_type, $import_type);
          }

          $file = fopen($uri, 'r');
          \Drupal::logger('Import products function ')->warning('<pre><code>' . print_r($bundle, TRUE) . '</code><pre>');
          $import_name = explode('/', $save);
          fgetcsv($file);
          $context['results']['current_row'] = 3;
          if ($parameters->createProduct === TRUE) {
            touch($save . '/log.json');
            // touch($save . '/override_values.json');.
            file_put_contents($save . '/field_definitions.json', json_encode($fields, JSON_UNESCAPED_UNICODE));
          }
          if ($parameters->createProduct === TRUE) {
            $log = file_get_contents($save . '/log.json');
            if (!empty($log)) {
              $log = json_decode($log, TRUE);
            }
            else {
              $log = [];
            }
          }

          $current_stop = ftell($file);

          $entities = [];
          $entity = [];

          $limit_entities = 50;
          /*            $override_values = [];
          if ($parameters->createProduct === TRUE) {
          $override_values = file_get_contents($save . '/override_values.json');
          if (empty($override_values)) {
          $override_values = [];
          }
          else {
          $override_values = json_decode($override_values, TRUE);
          }
          }
          // Getting data from CSV.
          $override_values_formatted = [];
          $temp_override_values = [];*/
          // IMage field indexes prepared.
          $image_fields_indexes = [];
          foreach ($fields[$import_type] as $entity_field_data) {
            if ($entity_field_data['field_types'] == 'image') {
              $image_fields_indexes[$import_type][] = $entity_field_data['index'];
            }
            if ($entity_field_data['field_types'] == 'path') {
              $path_field_index = $entity_field_data['index'];
            }
          }
          if ($import_type == 'product') {
            foreach ($fields[$sub_type] as $subtype_field_data) {
              if ($subtype_field_data['field_types'] == 'image') {
                $image_fields_indexes['variation'][] = $subtype_field_data['index'];
              }
            }
          }
          else {
            if (isset($fields[$sub_type])) {
              foreach ($fields[$sub_type] as $sub_type_bundle => $bundle_field_data) {
                foreach ($bundle_field_data as $subtype_field_data) {
                  if ($subtype_field_data['field_types'] == 'image') {
                    $image_fields_indexes['paragraph'][] = $subtype_field_data['index'];
                  }
                }
              }
            }
          }

          while (($line = fgetcsv($file)) !== FALSE) {
            if (strlen(implode($line)) != 0) {
              if (!empty($line[0])) {
                if (empty($entity)) {
                  $entity['row'] = $context['results']['current_row'];
                  if ($import_type == 'node') {
                    $entity[$import_type] = array_slice($line, 0, $first_subtype_index[0]['start']);
                    foreach ($first_subtype_index as $paragraph_index_data) {
                      $temp_paragraph = array_slice($line, $paragraph_index_data['start'], $paragraph_index_data['length'], TRUE);
                      if (array_filter($temp_paragraph)) {
                        $entity[$subtype_plural][] = $temp_paragraph;
                      }
                    }
                  }
                  else {
                    $entity[$import_type] = array_slice($line, 0, $first_subtype_index);
                    $entity[$subtype_plural][] = array_slice($line, $first_subtype_index, count($line), TRUE);
                  }
                }
                else {
                  $entities[] = $entity;
                  // $override_values_formatted[] = $temp_override_values;
                  if (count($entities) == $limit_entities) {
                    $entity = [];
                    // $temp_override_values = [];
                    break;
                  }
                  $entity = [];
                  // $temp_override_values = [];
                  $entity['row'] = $context['results']['current_row'];
                  if ($import_type == 'node') {
                    $entity[$import_type] = array_slice($line, 0, $first_subtype_index[0]['start']);
                    foreach ($first_subtype_index as $paragraph_index_data) {
                      $temp_paragraph = array_slice($line, $paragraph_index_data['start'], $paragraph_index_data['length'], TRUE);
                      if (array_filter($temp_paragraph)) {
                        $entity[$subtype_plural][] = $temp_paragraph;
                      }
                    }
                  }
                  else {
                    $entity[$import_type] = array_slice($line, 0, $first_subtype_index);
                    $entity[$subtype_plural][] = array_slice($line, $first_subtype_index, count($line), TRUE);
                  }
                }
              }
              else {
                if ($import_type == 'node') {
                  foreach ($first_subtype_index as $paragraph_index_data) {
                    $temp_paragraph = array_slice($line, $paragraph_index_data['start'], $paragraph_index_data['length'], TRUE);
                    if (!empty(array_filter($temp_paragraph))) {
                      $entity[$subtype_plural][] = $temp_paragraph;
                    }
                  }
                }
                else {
                  $entity[$subtype_plural][] = array_slice($line, $first_subtype_index, count($line), TRUE);
                }
              }
            }
            $context['results']['current_row']++;
            $current_stop = ftell($file);
          }
          if (!empty($entity)) {
            $entities[] = $entity;
          }
          $all_entities[$bundle]['entities'] = $entities;
          // $all_entities[$bundle]['override'] = $override_values_formatted;
          $all_entities[$bundle]['fields'] = $fields;
          $all_entities[$bundle]['parameters'] = $importParameters;
          $all_entities[$bundle]['external_folders'] = $external_folders;
          fclose($file);
          $final_result_log = [];
          // Putting error log.
          if ($parameters->createProduct === FALSE) {
            file_put_contents($save . '/log.json', json_encode($log, JSON_UNESCAPED_UNICODE));
          }
        }
        // kint($all_entities);exit;
        foreach ($all_entities as $bundleName => $bundleData) {
          foreach ($bundleData['entities'] as $key => $bundleEntity) {
            if ($bundleEntity[$import_type][$path_field_index] == 'en') {
              $bundleEntity[$import_type][$path_field_index] = '';
            }
            /*
            $context['results'] = [];
            $this->contentExportImportService->createNewEntity(
            $bundleData['fields'],
            $bundleEntity,
            $bundleData['parameters'],
            $bundleData['external_folders'],
            $bundleName,
            $import_type,
            $sub_type,
            $operation_mode,
            $context
            );exit;
             */
            // *
            $batch['operations'][] = [[$this->contentExportImportService, 'createNewEntity'],
            [
              $bundleData['fields'],
              $bundleEntity,
              $bundleData['parameters'],
              $bundleData['external_folders'],
              $bundleName,
              $import_type,
              $sub_type,
              $operation_mode,
            ],
            ];
            // */
          }
        }
      }
      else {
        \Drupal::messenger()->addMessage('There is some issue while reading your file. Please check with administrator');
      }
    }
    batch_set($batch);
  }

  /**
   * Searches for first field of variation.
   */
  public static function getFirstSubTypeIndex($fields, $subtype, $type) {
    // kint($fields);exit;
    $index = FALSE;
    $indexes = [];
    $count = 0;
    if (!empty($fields[$subtype])) {
      if ($type == 'product') {
        foreach ($fields[$subtype] as $field) {
          if ($field['machine_names'] == 'status' || $field['machine_names'] == 'variation_id') {
            $index = $field['index'];
            return $index;
          }
        }
      }
      else {
        foreach ($fields[$subtype] as $bundle => $bundleFieldData) {
          foreach ($bundleFieldData as $field) {
            if ($field['machine_names'] == 'type') {
              $indexes[] = ['start' => $field['index'], 'length' => count($bundleFieldData)];
              $count++;
            }
          }
        }
      }
    }
    else {
      $last_field = end($fields[$type]);
      $index = $last_field['index'] + 1;
    }
    if ($type == 'product') {
      return $index;
    }
    else {
      return $indexes;
    }

  }

  /**
   * Called after check batch finishes.
   */
  public function importFinishedCallback($success, $results, $operations) {
    foreach ($results['messages'] as $entity_msg) {
      \Drupal::messenger()->addMessage($entity_msg);
    }
  }

}
